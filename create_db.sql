
/* sous hypothèse que le périmètre du projet est local à un pays on peut déduire les dépendances fonctionnelles suivantes code postal -> ville => donc code postal et une clé primaire pour la table code postale id -> rue, id -> rue, id -> nom rue, id -> code postal => id et une clé pour la table adresse sous hypothèse qu'id soit unique et nom id-> surface id -> adresse => id et une cle pour la table parcelle sous hypothèse qu'id soit unique et non null id -> ça, id -> datecreation id -> statut id -> parcelle id est une primaire pour la table projet sous hypothèse qu'id soit unique et NOT NULL ainsi on obtient une base de données en forme normale de Boyce Codd, donc pas de redondance et st pas de parte d'information
*/

DROP TABLE IF EXISTS codepostal CASCADE;
DROP TABLE IF EXISTS adresse CASCADE;
DROP TABLE IF EXISTS parcelle CASCADE;
DROP TABLE IF EXISTS projet CASCADE;
DROP TYPE IF EXISTS statutprojet;

CREATE TABLE codepostal(
    codepostal integer NOT NULL,
    ville varchar(100) NOT NULL,
    PRIMARY KEY(codepostal)
);

CREATE TABLE adresse(
    idadr varchar(100), 
    rue integer NOT NULL CHECK (0<rue),
    nomrue varchar(100) NOT NULL,
    codepostal integer NOT NULL REFERENCES codepostal(codepostal),
    PRIMARY KEY (idadr) 
);

CREATE TABLE parcelle(
    idparcelle varchar(100),
    surface decimal NOT NULL CHECK (0<surface), 
    adresse varchar (100) NOT NULL UNIQUE REFERENCES adresse(idadr) on delete cascade on update cascade ,
    PRIMARY KEY (idparcelle) 
);

CREATE TYPE statutprojet AS ENUM ('ENCOURS', 'TERMINE,', 'ABONDONNE');

CREATE TABLE projet(
    idprojet varchar(100),
    nom  varchar(100) not null,
    ca decimal NOT NULL CHECK (0<ca),
    datecreation TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    statut statutprojet NOT NULL,
    parcelle varchar(100) NOT NULL REFERENCES parcelle (idparcelle) on delete cascade on update cascade ,
    PRIMARY KEY(idprojet)
);

CREATE INDEX index_adresse_ville on codepostal(ville);
CREATE INDEX index_adresse_codepostal on adresse(codepostal);

/*

SELECT * FROM projet pr
inner join parcelle par on (pr.parcelle = par.idparcelle) 
inner join adresse adr on (par.adresse = adr.idadr) 
inner join codepostal cp on (cp.codepostal=adr.codepostal); 

SELECT nom,ca,datecreation, statut,surface, rue, nomrue,cp.codepostal,ville 
FROM projet pr
inner join parcelle par on (pr.parcelle = par.idparcelle)
inner join adresse adr on (par.adresse = adr.idadr) 
inner join codepostal cp on (cp.codepostal=adr.codepostal)
where lower(cp.ville)  like '%montreuil%'

select statut, sum (ca)
from projet
group by statut


*/
