from flask import Flask, jsonify, request, Response, Blueprint
from .models import db
from .models.projet import Projet
from .models.parcelle import Parcelle


app = Flask(__name__)
app.config.from_object("project.config.Config")
db.init_app(app)
parcelleAPI = Blueprint('parcelleAPI', __name__)
ProjectAPI = Blueprint('ProjectAPI', __name__)


@ProjectAPI.route('', methods=['GET'])
def getProjects():
    return jsonify(Projet.getAll())


@ProjectAPI.route('/<id>', methods=['GET'])
def getProjectById(id):
    return_value = Projet.findById(id)
    return jsonify(return_value)


@ProjectAPI.route('', methods=['POST'])
def newProject():
    request_data = request.get_json()
    Projet.create(request_data["nom"], request_data["parcelle"],
                  request_data["ca"], request_data["status"])
    response = Response("Project added", 201, mimetype='text/plain')
    return response


@ProjectAPI.route('/<id>', methods=['PUT'])
def updateProject(id):
    request_data = request.get_json()
    Projet.update(id, request_data["nom"], request_data["parcelle"],
                  request_data["ca"], request_data["status"])
    response = Response("Projet updated", status=200,
                        mimetype='application/json')
    return response


@parcelleAPI.route('', methods=['GET'])
def getParcelles():
    return jsonify(Parcelle.getAll())


@parcelleAPI.route('/<id>', methods=['GET'])
def getParcelleById(id):
    return_value = Parcelle.findById(id)
    return jsonify(return_value)


@parcelleAPI.route('', methods=['POST'])
def addParcelle():
    request_data = request.get_json()
    Parcelle.create(request_data["adresse"], request_data["codepostal"],
                    request_data["ville"], request_data["surface"])
    response = Response("Parcelle added", 201, mimetype='text/plain')
    return response


@parcelleAPI.route('/<id>', methods=['PUT'])
def updateParcelle(id):
    request_data = request.get_json()
    Parcelle.update(id, request_data["adresse"], request_data["codepostal"],
                    request_data["ville"], request_data["surface"])
    response = Response("Parcelle updated", status=200,
                        mimetype='application/json')
    return response
