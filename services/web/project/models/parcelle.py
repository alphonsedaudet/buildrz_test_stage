
from . import db
import uuid

class Parcelle (db.Model):
    __tablename__ = "parcelle"
    id = db.Column(db.String(100),  primary_key=True)
    adresse = db.Column(db.String(100), nullable=False)
    codepostal = db.Column(db.Integer, nullable=False)
    ville = db.Column(db.String(100), nullable=False)
    surface = db.Column(db.Numeric(), nullable=False)

    def parse(self):
        return {'id': self.id, 'adresse': self.adresse, 'codepostal': self.codepostal, 'ville': self.ville, 'surface': str(self.surface)}

    def create(_adresse, _codepostal, _ville, _surface):
        newParcelle = Parcelle(id=uuid.uuid4(), adresse=_adresse,codepostal=_codepostal, ville=_ville, surface=_surface)
        db.session.add(newParcelle)
        db.session.commit()

    def getAll():
        return [Parcelle.parse(parcelle) for parcelle in Parcelle.query.all()]

    def findById(_id):
        return Parcelle.parse(Parcelle.query.filter_by(id=_id).first())

    def update(_id, _adresse, _codepostal, _ville, _surface):
        parcelle_to_update = Parcelle.query.filter_by(id=_id).first()
        parcelle_to_update.adresse = _adresse
        parcelle_to_update.codepostal = _codepostal
        parcelle_to_update.ville = _ville
        parcelle_to_update.surface = _surface
        db.session.commit()
        return Parcelle.parse(parcelle_to_update)

    def delete(_id):
        Parcelle.query.filter_by(id=_id).delete()
        db.session.commit()
