
from . import db
import uuid
import datetime

class Projet (db.Model):
    __tablename__ = "projet"
    id = db.Column(db.String(100),  primary_key=True)
    nom = db.Column(db.String(100),  nullable=True)
    ca = db.Column(db.Numeric(), nullable=True)
    datecreation = db.Column(db.DateTime(),  nullable=True)
    status = db.Column(db.String(100),  nullable=True)
    parcelle = db.Column(db.String(100), db.ForeignKey('parcelle.id'), nullable=False)

    def parse(self):
        return {'id': self.id, 'nom': self.nom, 'parcelle': self.parcelle, 'ca': str(self.ca), 'datecreation': self.datecreation, 'status': self.status}

    def create(_nom, _parcelle, _ca, _status):
        newProjet = Projet(id=uuid.uuid4(), nom=_nom, parcelle=_parcelle,ca=_ca, datecreation=datetime.datetime.now(), status=_status)
        db.session.add(newProjet)
        db.session.commit()

    def getAll():
        return [Projet.parse(projet) for projet in Projet.query.all()]

    def findById(_id):
        return Projet.parse(Projet.query.filter_by(id=_id).first())

    def update(_id, _nom, _parcelle, _ca, _status):
        project_to_update = Projet.query.filter_by(id=_id).first()
        project_to_update.nom = _nom
        project_to_update.parcelle = _parcelle
        project_to_update.ca = _ca
        project_to_update.status = _status
        db.session.commit()
        return Projet.parse(project_to_update)

    def delete(_id):
        Projet.query.filter_by(id=_id).delete()
        db.session.commit()
