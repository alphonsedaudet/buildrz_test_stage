from flask import Flask, jsonify
from .models import db
from .api import ProjectAPI, parcelleAPI

app = Flask(__name__)
app.config.from_object("project.config.Config")
db.init_app(app)

app.register_blueprint(ProjectAPI, url_prefix='/api/v1/projet')
app.register_blueprint(parcelleAPI, url_prefix='/api/v1/parcelle')

@app.route("/")
def hello_world():
    return jsonify(hello="world")

if __name__ == "__main__":
    app.run(port=1234, debug=True)
