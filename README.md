# buildrz_test_stage


## base de données

un shéme de base de données normalisé à la FNBC est à la racine du projet

## installation

 il faut installer  Docker Daemon et Docker Compose

- cloner le pojet <<git clone ... >> 
- cd buildrz_test_stage
- docker-compose build
- docker compose up -d
- docker exec -ti web bash
- python manage.py create_bd


## Client HTTP

j'ai fait le choix de produire une documentation OPEN API pour documenter les endpoints et tester les services web Restfull.
une fois que le service swagger-ui présent dans le fichier docker-compose.yaml est lancé, la documentation Swagger ui est accisible depuis le port 8082;

- https://gitlab.com/alphonsedaudet/buildrz_test_stage/-/blob/master/swagger/openapi.json

#### http://localhost:8082/#/ 
